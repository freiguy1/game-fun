
## Collision notes _(this is old and incorrect)_

For collisions, I use the words _inset_ and _offset_. Here's a visual representation of what they mean. In this text-picture, the lines show the outside of the item we're checking collisions for and the dots show the collision points we're checking. Keep in mind, in text, a horizontal space is less wide than a new line is tall, so it looks a little skewed.

```
_______________________________
|    .                   .    |
|                             |
|                             |
|.                           .|
|                             |
|                             |
|                             |
|                             |
|                             |
|.                           .|
|                             |
|                             |
|                             |
|____.___________________.____|

```

Here, the _inset_ would be 1 (ish) because the collision point dots are 1 (ish) away from the edge of the box.
The _offset_ would be 4, because the collision point dots are 4 away from the adjacent sides of the box.