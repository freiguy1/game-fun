use ggez::graphics;
use ggez::mint::{Vector2, Point2};
use ggez::{Context, GameResult};

pub trait Projectile {
    fn get_velocity(&self) -> Vector2<f32> {
        Vector2 { x: 0., y: 0. }
    }

    // Radius of projectile circle
    fn get_radius(&self) -> f32 {
        0.
    }

    fn get_location(&self) -> Point2<f32> {
        Point2 { x: 0., y: 0. }
    }

    fn get_damage(&self) -> f32 {
        0.
    }

    fn draw(&self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    fn update_location(&mut self, level: &::level::Level);

    fn has_expired(&self) -> bool { false }
}

pub struct GooProjectile {
    location: Point2<f32>,
    velocity: Vector2<f32>,
    distance_left: f32
}

impl GooProjectile {
    pub fn new(location: Point2<f32>, velocity: Vector2<f32>) -> GooProjectile {
        GooProjectile {
            location,
            velocity,
            distance_left: 50.
        }
    }

    fn get_feet_per_tick(&self) -> f32 {
        (self.velocity.x.powi(2) + self.velocity.y.powi(2)).sqrt()
    }
}

impl Projectile for GooProjectile {
    fn get_velocity(&self) -> Vector2<f32> {
        self.velocity
    }

    // Radius of projectile circle
    fn get_radius(&self) -> f32 {
        1.
    }

    fn get_location(&self) -> Point2<f32> {
        Point2 { x: 0., y: 0. }
    }

    fn get_damage(&self) -> f32 {
        0.
    }

    fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        let color = graphics::Color::from_rgb_u32(0x6aba8c);
        // let draw_location = Point2 { x: self.location.x, y: self.location.y };
        let mesh = graphics::Mesh::new_circle(ctx, graphics::DrawMode::fill(), self.location, self.get_radius(), 0.1, color)?;
        graphics::draw(ctx, &mesh, (Point2 { x: 0.0, y: 0.0 },)) 
    }

    fn update_location(&mut self, _level: &::level::Level) {
        self.location.x = self.location.x + self.velocity.x;
        self.location.y = self.location.y + self.velocity.y;
        self.distance_left -= self.get_feet_per_tick();
    }

    fn has_expired(&self) -> bool { self.distance_left <= 0. }
}
