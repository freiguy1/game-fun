use ggez::{Context, GameResult};
use ggez::mint::Point2;
use ggez::graphics::{self, Rect};

const TILE_SIZE: f32 = 8.0;

pub struct Level {
    tiles: Vec<bool>,
    width: usize
}

impl Level {
    pub fn level_1() -> Level {
        let tiles: Vec<bool> = vec![
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
            true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
            true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
            true, false, false, false, false, false, false, false, true, false, false, false, false, false, false, true,
            true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
            true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true
        ];
        Level { tiles: tiles, width: 16 }
    }

    pub fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        let color = graphics::Color::from_rgb_u32(0xFFFFFF);
        for (i, _) in self.tiles.iter().enumerate().filter(|&(_, &t)| t) {
            let x = i % self.width;
            let y = i / self.width;
            let x_pixels = x as f32 * TILE_SIZE;
            let y_pixels = y as f32 * TILE_SIZE;
            let rect = graphics::Rect::new(x_pixels, y_pixels, TILE_SIZE, TILE_SIZE);
            let mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), rect, color)?;
            graphics::draw(ctx, &mesh, (Point2 { x: 0.0, y: 0.0 },))?;
        };
        Ok(())
    }

    #[allow(dead_code)]
    pub fn obstacles_at_rect(&self, rect: &Rect) -> Vec<Rect> {
        let points = ::util::collision::rect_to_points(rect);
        points.iter().filter_map(|p| {
            let tile_x = (p.x / TILE_SIZE).floor() as usize;
            let tile_y = (p.y / TILE_SIZE).floor() as usize;
            let tile_index = tile_y * self.width + tile_x;
            if self.tiles[tile_index] {
                Some(Rect::new(
                    tile_x as f32 * TILE_SIZE,
                    tile_y as f32 * TILE_SIZE,
                    TILE_SIZE,
                    TILE_SIZE))
            } else { None }
        }).collect()
    }
}
