use ggez::event::KeyCode;
use ggez::mint::Vector2;
use std::collections::HashSet;

use super::Cardinal;

pub fn cardinal_to_velocity(cardinal: &Option<Cardinal>, velocity: f32) -> Vector2<f32> {
    if let &Some(ref cardinal) = cardinal {
        let diagonal_velocity = velocity  * 0.7071;
        match cardinal {
            &Cardinal::North => Vector2 { x: 0.0, y: -velocity },
            &Cardinal::South => Vector2 { x: 0.0, y: velocity },
            &Cardinal::East => Vector2 { x: velocity, y: 0.0 },
            &Cardinal::West => Vector2 { x: -velocity, y: 0.0 },
            &Cardinal::NorthEast => Vector2 { x: diagonal_velocity, y: -diagonal_velocity },
            &Cardinal::NorthWest => Vector2 { x: -diagonal_velocity, y: -diagonal_velocity },
            &Cardinal::SouthEast => Vector2 { x: diagonal_velocity, y: diagonal_velocity },
            &Cardinal::SouthWest => Vector2 { x: -diagonal_velocity, y: diagonal_velocity },
        }
    } else {
        Vector2 { x: 0.0, y: 0.0 }
    }
}

const UP_KEYS: [KeyCode; 2] = [KeyCode::W, KeyCode::Up];
const DOWN_KEYS: [KeyCode; 2] = [KeyCode::S, KeyCode::Down];
const LEFT_KEYS: [KeyCode; 2] = [KeyCode::A, KeyCode::Left];
const RIGHT_KEYS: [KeyCode; 2] = [KeyCode::D, KeyCode::Right];

pub fn direction_keys_to_cardinal(keys_down: &HashSet<KeyCode>) -> Option<Cardinal> {
    fn is_up_not_down(keys_down: &HashSet<KeyCode>) -> bool {
        keys_down.iter().any(|kc| UP_KEYS.iter().any(|uk| uk == kc))
            && !keys_down.iter().any(|kc| DOWN_KEYS.iter().any(|uk| uk == kc))
    }
    fn is_down_not_up(keys_down: &HashSet<KeyCode>) -> bool {
        keys_down.iter().any(|kc| DOWN_KEYS.iter().any(|uk| uk == kc))
            && !keys_down.iter().any(|kc| UP_KEYS.iter().any(|uk| uk == kc))
    }
    fn is_left_not_right(keys_down: &HashSet<KeyCode>) -> bool {
        keys_down.iter().any(|kc| LEFT_KEYS.iter().any(|uk| uk == kc))
            && !keys_down.iter().any(|kc| RIGHT_KEYS.iter().any(|uk| uk == kc))
    }
    fn is_right_not_left(keys_down: &HashSet<KeyCode>) -> bool {
        keys_down.iter().any(|kc| RIGHT_KEYS.iter().any(|uk| uk == kc))
            && !keys_down.iter().any(|kc| LEFT_KEYS.iter().any(|uk| uk == kc))
    }

    let direction_keys = [UP_KEYS, DOWN_KEYS, LEFT_KEYS, RIGHT_KEYS].concat();

    let filtered_keys: HashSet<KeyCode> = keys_down.iter().filter(|kc|
        direction_keys.iter().any(|dk| dk == *kc)).map(|kc| *kc).collect();

    if is_up_not_down(&filtered_keys) {
        if is_left_not_right(&filtered_keys) {
            Some(Cardinal::NorthWest)
        } else if is_right_not_left(&filtered_keys) {
            Some(Cardinal::NorthEast)
        } else {
            Some(Cardinal::North)
        }
    } else if is_down_not_up(&filtered_keys) {
        if is_left_not_right(&filtered_keys) {
            Some(Cardinal::SouthWest)
        } else if is_right_not_left(&filtered_keys) {
            Some(Cardinal::SouthEast)
        } else {
            Some(Cardinal::South)
        }
    } else if is_left_not_right(&filtered_keys) {
        Some(Cardinal::West)
    } else if is_right_not_left(&filtered_keys) {
        Some(Cardinal::East)
    } else {
        None
    }
}

pub mod collision {
    use ggez::mint::{Vector2, Point2};
    use ggez::graphics::Rect;

    pub fn rect_to_points(r: &Rect) -> Vec<Point2<f32>> {
        vec![
            Point2 { x: r.x, y: r.y },
            Point2 { x: r.x, y: r.y + r.h },
            Point2 { x: r.x + r.w, y: r.y },
            Point2 { x: r.x + r.w , y: r.y + r.h }
        ]
    }


    // type Line = (Point2, Point2);

    // fn rect_to_lines(r: &Rect) -> Vec<Line> {
    //     vec![
    //         (Point2::new(r.x, r.y), Point2::new(r.x + r.w, r.y)),
    //         (Point2::new(r.x, r.y), Point2::new(r.x, r.y + r.h)),
    //         (Point2::new(r.x + r.w, r.y), Point2::new(r.x + r.w, r.y + r.h)),
    //         (Point2::new(r.x , r.y + r.h), Point2::new(r.x + r.w, r.y + r.h))
    //     ]
    // }

    // pub fn do_lines_intersect(l1: &Line, l2: &Line) -> Option<Point2> {
    //     let s1_x = l1.1.x - l1.0.x;
    //     let s1_y = l1.1.y - l1.0.y;
    //     let s2_x = l2.1.x - l2.0.x;
    //     let s2_y = l2.1.y - l2.0.y;

    //     let s = (-s1_y * (l1.0.x - l2.0.x) + s1_x * (l1.0.y - l2.0.y)) / (-s2_x * s1_y + s1_x * s2_y);
    //     let t = ( s2_x * (l1.0.y - l2.0.y) - s2_y * (l1.0.x - l2.0.x)) / (-s2_x * s1_y + s1_x * s2_y);

    //     if s >= 0. && s <= 1. && t >= 0. && t <= 1. {
    //         // Collision detected
    //         let x = l1.0.x + (t * s1_x);
    //         let y = l1.0.y + (t * s1_y);
    //         Some(Point2::new(x, y))
    //     } else { None }
    // }

    pub fn max_possible_velocity(r: &Rect, velocity: &Vector2<f32>, obstacles: &Vec<Rect>) -> Vector2<f32> {
        let r_shifted_x = Rect::new(r.x + velocity.x, r.y, r.w, r.h);
        let r_shifted_y = Rect::new(r.x, r.y + velocity.y, r.w, r.h);

        let mut result = velocity.clone();

        for obstacle in obstacles.iter() {
            if velocity.x != 0. && rect_overlaps(&r_shifted_x, &obstacle) {
                if velocity.x > 0. {
                    let dist_between = obstacle.left() - r.right();
                    if dist_between < result.x { result.x = dist_between }
                } else {
                    let dist_between = r.left() - obstacle.right();
                    if -dist_between > result.x { result.x = -dist_between }
                }
            }
            if velocity.y != 0. && rect_overlaps(&r_shifted_y, &obstacle) {
                if velocity.y > 0. && r_shifted_y.bottom() != obstacle.top() {
                    let dist_between = obstacle.top() - r.bottom();
                    if dist_between < result.y { result.y = dist_between }
                } else {
                    let dist_between = r.top() - obstacle.bottom();
                    if -dist_between > result.y { result.y = -dist_between }
                }
            }
        }

        result
    }

    pub fn rect_overlaps(r1: &Rect, r2: &Rect) -> bool {
        r1.left() < r2.right() && r1.right() > r2.left() && r1.top() < r2.bottom()
            && r1.bottom() > r2.top()
    }

    #[test]
    fn max_possible_velocity_test_x_pos() {
        let rect = Rect::new_i32(0, 0, 10, 10);
        let velocity = Vector2 { x: 6., y: 0. };
        let obstacles = vec![
            Rect::new_i32(12, 0, 10, 10)
        ];
        let actual = max_possible_velocity(&rect, &velocity, &obstacles);
        let expected = Vector2 { x: 2., y: 0. };
        assert_eq!(actual, expected);
    }

    #[test]
    fn max_possible_velocity_test_x_neg() {
        let rect = Rect::new_i32(0, 0, 10, 10);
        let velocity = Vector2 { x: -6., y: 0. };
        let obstacles = vec![
            Rect::new_i32(-10, 0, 8, 8)
        ];
        let actual = max_possible_velocity(&rect, &velocity, &obstacles);
        let expected = Vector2 { x: -2., y: 0. };
        assert_eq!(actual, expected);
    }

    // #[test]
    // fn do_lines_intersect_test() {
    //     let l1 = (Point2::new(0., 5.), Point2::new(0., -5.));
    //     let l2 = (Point2::new(-5., 0.), Point2::new(5., 0.));
    //     let actual = do_lines_intersect(&l1, &l2);
    //     let expected = Some(Point2::new(0., 0.));
    //     assert_eq!(actual, expected);
    // }

    // #[test]
    // fn do_lines_intersect_colinear_test() {
    //     let l1 = (Point2::new(0., 5.), Point2::new(0., -5.));
    //     let l2 = (Point2::new(0., 5.), Point2::new(0., -5.));
    //     let actual = do_lines_intersect(&l1, &l2);
    //     let expected = None;
    //     assert_eq!(actual, expected);
    // }

    // #[test]
    // fn do_lines_intersect_parallel_test() {
    //     let l1 = (Point2::new(0., 5.), Point2::new(0., -5.));
    //     let l2 = (Point2::new(1., 5.), Point2::new(1., -5.));
    //     let actual = do_lines_intersect(&l1, &l2);
    //     let expected = None;
    //     assert_eq!(actual, expected);
    // }
}
