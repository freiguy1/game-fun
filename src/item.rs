use ggez::graphics::{self, Rect};
use ggez::mint::Point2;
use ggez::{Context, GameResult};

pub const SIZE: f32 = 2.0;

pub struct Item {
    pub location: Point2<f32>
}

impl Item {
    pub fn new(x: f32, y: f32) -> Item {
        Item {
            location: Point2 { x: x, y: y }
        }
    }

    pub fn draw(&self, ctx: &mut Context, camera: &Point2<f32>) -> GameResult<()> {
        let color = graphics::Color::from_rgb_u32(0xbb2222);
        let rect = self.get_draw_rect(camera);
        let mesh =
            graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), rect, color)?;
        graphics::draw(ctx, &mesh, (Point2 { x: 0.0, y: 0.0 },)) 
    }

    fn get_draw_rect(&self, camera: &Point2<f32>) -> Rect {
        graphics::Rect::new(self.location.x - camera.x - SIZE / 2.0,
                            self.location.y - camera.y - SIZE / 2.0,
                            SIZE,
                            SIZE)
    }
}
