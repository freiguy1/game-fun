use ggez::event::KeyCode;
use ggez::{Context, GameResult};
use ggez::graphics;
use ggez::mint::{Point2, Vector2};
use std::collections::HashSet;

use util::*;

const DEFAULT_VELOCITY: f32 = 0.6;
pub const HEIGHT: f32 = 6.0;
pub const WIDTH: f32 = 4.0;

pub struct Player {
    pub location: Point2<f32>,
    pub velocity: Vector2<f32>
}

impl Player {
    pub fn new(x: f32, y: f32) -> Player {
        Player {
            location: Point2 { x: x, y: y },
            velocity: Vector2 { x: 0.0, y: 0.0 }
        }
    }


    pub fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let color = graphics::Color::from_rgb_u32(0x009999);
        let rect = self.get_rect(); 
        let mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), rect, color)?;
        graphics::draw(ctx, &mesh, (ggez::mint::Point2 { x: 0.0, y: 0.0 },)) 
    }

    fn get_rect(&self) -> graphics::Rect {
        graphics::Rect::new(
            self.location.x - WIDTH / 2.0,
            self.location.y - HEIGHT / 2.0,
            WIDTH,
            HEIGHT)
    }

    pub fn update_location(&mut self) {
        self.location.x = self.location.x + self.velocity.x;
        self.location.y = self.location.y + self.velocity.y;
    }

    pub fn update_velocity(&mut self, keys_down: &HashSet<KeyCode>, level: &::level::Level) {
        let cardinal = direction_keys_to_cardinal(keys_down);
        let intended_velocity = cardinal_to_velocity(&cardinal, DEFAULT_VELOCITY);
        let projected_rect = graphics::Rect::new(
            (self.location.x + intended_velocity.x) - WIDTH / 2.,
            (self.location.y + intended_velocity.y) - HEIGHT / 2.,
            WIDTH,
            HEIGHT);

        let obstacles = level.obstacles_at_rect(&projected_rect);
        let rect = self.get_rect();
        self.velocity = ::util::collision::max_possible_velocity(&rect, &intended_velocity, &obstacles);
    }
}
