extern crate ggez;

mod player;
mod util;
mod level;
mod item;
mod projectile;

use ggez::event::{self, KeyCode, KeyMods, MouseButton};
use ggez::{Context, GameResult};
use ggez::graphics;
use ggez::mint::{Point2, Vector2};
use std::collections::HashSet;

use player::Player;
use level::Level;
use item::Item;
use projectile::Projectile;


const PIXELS_PER_FOOT: f32 = 6.;

#[derive(PartialEq)]
pub enum Cardinal {
    North,
    South,
    East,
    West,
    NorthEast,
    SouthEast,
    NorthWest,
    SouthWest
}

// First we make a structure to contain the game's state
struct MainState {
    frames: usize,
    player: Player,
    keys_down: HashSet<KeyCode>,
    level: Level,
    item: Item,
    projectiles: Vec<Box<dyn Projectile>>
}

impl MainState {
    fn new(_ctx: &mut Context) -> GameResult<MainState> {
        let player = Player::new(15.0, 15.0);
        let item = Item::new(80.0, 30.0);
        let s = MainState {
            frames: 0,
            player: player,
            keys_down: HashSet::new(),
            level: Level::level_1(),
            item: item,
            projectiles: Vec::new()
        };
        Ok(s)
    }

    fn is_player_overlapping_item(&self) -> bool {
        let player_rect = graphics::Rect::new(self.player.location.x - player::WIDTH / 2.0,
                                              self.player.location.y - player::HEIGHT / 2.0,
                                              player::WIDTH,
                                              player::HEIGHT);
        let item_rect = graphics::Rect::new(self.item.location.x - item::SIZE / 2.0,
                                            self.item.location.y - item::SIZE / 2.0,
                                            item::SIZE,
                                            item::SIZE);
        player_rect.overlaps(&item_rect)
    }

    fn center_on_player(&self, ctx: &mut Context) -> GameResult<()> {
        let drawable_size_feet = drawable_size_feet(&ctx);
        graphics::set_screen_coordinates(ctx, graphics::Rect::new(
            self.player.location.x - (drawable_size_feet.0 / 2.),
            self.player.location.y - (drawable_size_feet.1 / 2.),
            drawable_size_feet.0,
            drawable_size_feet.1))
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _: &mut Context) -> GameResult<()> {
        let level = &self.level;
        self.player.update_velocity(&self.keys_down, level);
        self.player.update_location();
        self.projectiles.iter_mut().for_each(|p| p.update_location(level));
        let mut expired_projectile_indexes = self.projectiles.iter()
            .enumerate()
            .filter_map(|(i, p)| if p.has_expired() { Some(i) } else { None }).collect::<Vec<_>>();
        while let Some(i) = expired_projectile_indexes.pop() {
            self.projectiles.swap_remove(i);
        }
        // self.projectiles = self.projectiles.drain(..).filter(|p| !p.has_expired()).collect();
        if self.is_player_overlapping_item() {
            println!("Player is overlapping item");
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let c = ggez::graphics::Color::from_rgb_u32(0x000000);
        graphics::clear(ctx, c);
        self.center_on_player(ctx)?;
        let camera = Point2 { x: 0., y: 0. };
        self.level.draw(ctx)?;
        self.item.draw(ctx, &camera)?;
        self.player.draw(ctx)?;
        for p in self.projectiles.iter_mut() {
            p.draw(ctx)?;
        }

        graphics::present(ctx)?;
        self.frames += 1;
        if (self.frames % 100) == 0 {
            println!("FPS: {}, projectiles.len: {}", ggez::timer::fps(ctx), self.projectiles.len());
        }
        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymod: KeyMods, repeat: bool) {
        if repeat { return; }
        self.keys_down.insert(keycode);
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymod: KeyMods) {
        self.keys_down.remove(&keycode);
    }

    fn mouse_button_down_event(&mut self, ctx: &mut Context, _button: MouseButton, pixel_x: f32, pixel_y: f32) {
        // First map pixels to feet, then shift according to camera loc (which is based on player loc).
        let drawable_size = graphics::drawable_size(ctx);
        let (x_feet, y_feet) = drawable_size_feet(&ctx);
        let x = pixel_x * x_feet / drawable_size.0 + (self.player.location.x - (x_feet / 2.0));
        let y = pixel_y * y_feet / drawable_size.1 + (self.player.location.y - (y_feet / 2.0));
        let speed = 3.;
        let direction_vector = Vector2 {
            x: x - self.player.location.x,
            y: y - self.player.location.y
        };
        let direction_length = (direction_vector.x.powi(2) + direction_vector.y.powi(2)).sqrt();
        let direction_divisor = direction_length / speed;
        let velocity = Vector2 {
            x: (direction_vector.x / direction_divisor) + self.player.velocity.x,
            y: (direction_vector.y / direction_divisor) + self.player.velocity.y
        };
        let projectile = Box::new(projectile::GooProjectile::new(self.player.location, velocity));
        self.projectiles.push(projectile);
    }

    fn resize_event(&mut self, ctx: &mut Context, _width: f32, _height: f32) {
        let _ = self.center_on_player(ctx);
    }
}

fn drawable_size_feet(ctx: &Context) -> (f32, f32) {
    let drawable_size = graphics::drawable_size(&ctx);
    (drawable_size.0 / PIXELS_PER_FOOT, drawable_size.1 / PIXELS_PER_FOOT)
}

pub fn main() -> GameResult {
    // let c = if std::path::Path::new("config.toml").exists() {
    //     let mut f = std::fs::File::open("config.toml").expect("couldn't open config.toml");
    //     conf::Conf::from_toml_file(&mut f).expect("couldn't parse config.toml")
    // } else {
    //     let c = conf::Conf::new();
    //     let mut config_file = std::fs::File::create("config.toml").expect("couldn't open config.toml");
    //     c.to_toml_file(&mut config_file).expect("couldn't write to config file");
    //     c
    // };

    // let mut ctx = &mut Context::load_from_conf("thing", "Ethan", c).unwrap();

    // We add the CARGO_MANIFEST_DIR/resources do the filesystems paths so
    // we we look in the cargo project for files.
    // if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
    //     let mut path = path::PathBuf::from(manifest_dir);
    //     path.push("resources");
    //     ctx.filesystem.mount(&path, true);
    // }

    let mut context_builder = ggez::ContextBuilder::new("thing", "Ethan");
    if let Ok(manifest_dir) = std::env::var("CARGO_MANIFEST_DIR") {
        context_builder = context_builder.add_resource_path(manifest_dir)
    }

    let (mut ctx, mut events_loop) = context_builder.build()?;
    let (x_feet, y_feet) = drawable_size_feet(&ctx);
    graphics::set_screen_coordinates(&mut ctx, graphics::Rect::new(0.0, 0.0, x_feet, y_feet))?;

    let state = &mut MainState::new(&mut ctx)?;
    event::run(&mut ctx, &mut events_loop, state)
}
